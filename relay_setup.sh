#! /bin/bash
# Setup user
USERNAME=`hostname -s`_sync
useradd $USERNAME
usermod -a -G lifespanraw,ozmt $USERNAME
cd /home/$USERNAME

userdel -r clone_user

# Setup ssh
mkdir -p .ssh
chmod 700 .ssh
chown $USERNAME:$USERNAME .ssh
timeout 2 su $USERNAME -c "ssh -o StrictHostKeyChecking=no root@192.168.2.2 echo hello"

# Setup data directory
chown -R ${USERNAME}:${USERNAME} /raw/data
chgrp lifespanraw /raw/data/staging
chown -R ${USERNAME}:${USERNAME} /raw/scripts

su $USERNAME -c "cd /raw/scripts/hcpxnat; git pull"

# Add scheduled task
if [ ! -f /var/spool/cron/${USERNAME} ]; then
    crontab -u $USERNAME /root/clone_setup/user_crontab
fi

crontab -e -u $USERNAME

# Generate SSH keys

if [ ! -f /home/${USERNAME}/.ssh/id_rsa.pub ]; then
    su $USERNAME -c "ssh-keygen -q -t rsa -b 4096 -f /home/${USERNAME}/.ssh/id_rsa"
fi
echo
echo "$USERNAME public ssh key:"
echo
cat /home/$USERNAME/.ssh/id_rsa.pub # Then paste into authorized_keys on aspera server
echo

# Install aspera client

if [ ! -d /home/${USERNAME}/.aspera ]; then
    echo "Installing aspera client"
    su $USERNAME -c "wget ftp://ftp.nrg.wustl.edu/pub/XNAT_relay/aspera-connect-3.6.2.117442-linux-64.tar.gz"
    su $USERNAME -c "tar zxvf aspera-connect-3.6.2.117442-linux-64.tar.gz"
    su $USERNAME -c "./aspera-connect-3.6.2.117442-linux-64.sh"
fi

# Configure rsync daemon

cat /root/clone_setup/rsyncd.conf | sed s/USERNAME/$USERNAME/g > /etc/rsyncd.conf

# Fix ozmt permissions

cd /opt/ozmt/;hg pull -u;chgrp -R ozmt /var/zfs_tools/;chmod -R g+srwX /var/zfs_tools/

cd /root/clone_setup

# Setup scripts authentication
if [ ! -f /home/${USERNAME}/.hcpxnat_intradb.cfg ]; then
    cp /root/clone_setup/.hcpxnat_intradb.cfg /home/${USERNAME}/.hcpxnat_intradb.cfg
    chown ${USERNAME}:${USERNAME} /home/${USERNAME}/.hcpxnat_intradb.cfg
    su $USERNAME -c "vim /home/${USERNAME}/.hcpxnat_intradb.cfg"
fi

if [ ! -f /home/${USERNAME}/.hcpxnat_relay.cfg ]; then
    cp /root/clone_setup/.hcpxnat_relay.cfg /home/${USERNAME}/.hcpxnat_relay.cfg
    chown ${USERNAME}:${USERNAME} /home/${USERNAME}/.hcpxnat_relay.cfg
    su $USERNAME -c "vim /home/${USERNAME}/.hcpxnat_relay.cfg"
fi

